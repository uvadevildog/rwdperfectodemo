import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class ParallelRWDTest {
	
	public static Map<String,String> testngParams;
	private static final String siteUnderTest = "http://nxc.co.il/demoaut/index.php";
	
	@BeforeSuite
	public void beforeSuite(ITestContext testContext){
		testngParams = testContext.getCurrentXmlTest().getAllParameters();
	}
	
	//Specify required parameters delimited by equal
	//[capability name]=[capability value], [name]=[value]...
	public DesiredCapabilities specifyCapabilities(String... delimitedCaps){
		DesiredCapabilities newCaps = new DesiredCapabilities();
		for (String addCap : delimitedCaps){
			String[] splitKeyValue = addCap.split("=");
			newCaps.setCapability(splitKeyValue[0], splitKeyValue[1]);
		}
		return newCaps;
	}
	
	//Add to Object Array to test different browsers
	@DataProvider(parallel = true)
	public Object[][] capabilitiesProvider() {
		return new Object[][] {
			new Object[] { "iPhone Test", specifyCapabilities("model=iPhone.*", "network=.*")},
			new Object[] { "Desktop FF Test", DesiredCapabilities.firefox()},
			new Object[] { "Android Test", specifyCapabilities("platformName=Android")},
			//new Object[] { "iPad Test", specifyCapabilities("model=iPad.*")},
			new Object[] { "Desktop Chrome", DesiredCapabilities.chrome()},
		};
	}
	
	@Test(dataProvider = "capabilitiesProvider")
	public void TestWebSite(String testName, DesiredCapabilities caps) {
		logThread("Run " + testName + " started");
		
		RemoteWebDriver driver = getNewDeviceDriver(caps);
		
		if (driver == null) {
			logThread(testName + " Failed because driver was not found");
			Assert.fail();
		}

		try {
			logThread(testName + " execution begun");
			
			driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			driver.manage().timeouts().pageLoadTimeout(20, TimeUnit.SECONDS);
			
			driver.get(siteUnderTest);
			
			driver.findElement(By.id("seat")).click();
			
			List<WebElement> seats = driver.findElements(By.xpath(".//span[contains(@class,'seat ')]"));
			Assert.assertEquals(seats.size(), 165);
			
			//Find all available seats and select random one
			//Make sure selected seat number appears on screen
			List<WebElement> availableSeats = driver.findElements(By.xpath(".//span[contains(@class,'available')]"));
			WebElement selectedSeat = availableSeats.get(new Random().nextInt(availableSeats.size()-1));
			String seatName = selectedSeat.getAttribute("id");		
			selectedSeat.click();
			String seatNumber = driver.findElement(By.className("paxseatlabel")).getText();
			Assert.assertEquals(seatName, seatNumber);
			
			driver.findElement(By.xpath("//*[text()='Back']")).click();
			
			//Login to web application - set user and password
			LoginToSUT(driver);
			
			//Text checkpoint on "Welcome back John"
			(new WebDriverWait(driver, 10)).until(new ExpectedCondition<Boolean>() {
			    public Boolean apply(WebDriver d) {
			        return d.findElement(By.cssSelector("#welcome>h3")) != null;
			    }
			});

			String welcomeText = driver.findElement(By.cssSelector("#welcome>h3")).getText();
			
			Assert.assertTrue(welcomeText.contains("Welcome back John"));
			
			logThread(testName + " execution ended successfully");
		} catch (Exception e) {
			logThread(testName + " execution FAILED with error:\n	 " + e.getMessage());
			Assert.fail(e.getMessage());
		}
		finally{
			closeAndQuit(driver);
		}
	}
	
	public void closeAndQuit(RemoteWebDriver driver) {
		if (driver != null) {
			try {
				driver.close();
				String windTunnelLink = driver.getCapabilities().getCapability("windTunnelReportUrl") + "";
				if (!windTunnelLink.isEmpty())
					logThread("WindTunnel Link:\n\t" + windTunnelLink);
			} catch (Exception e) {
				logThread("WindTunnel Report Not Available");
				e.printStackTrace();
			}
			finally{
				driver.quit();
				logThread("Driver quit");
			}
		}
	}
	
	private void LoginToSUT(RemoteWebDriver driver) {
		driver.findElement(By.name("username")).sendKeys("John");
		driver.findElement(By.name("password")).sendKeys("Perfecto1");
		driver.findElement(By.cssSelector(".login>div>button")).click();
	}
	
	public RemoteWebDriver getNewDeviceDriver(DesiredCapabilities specifiedCapabilities){
		
		String rwdUrl = null;
				
		if (StringUtils.isNotEmpty(specifiedCapabilities.getBrowserName()) && specifiedCapabilities.asMap().values().size() == 3) {
			updateCredsWarning("seleniumGridHost");
			rwdUrl = "http://" + testngParams.get("seleniumGridHost") + "/wd/hub";
		}
		else {
			updateCredsWarning("perfectoCloud");
			specifiedCapabilities.merge(new DesiredCapabilities("MobileOS", "", Platform.ANY));
			
			updateCredsWarning("perfectoUserName");
			updateCredsWarning("perfectoPassword");
			specifiedCapabilities.setCapability("user", testngParams.get("perfectoUserName"));
			specifiedCapabilities.setCapability("password", testngParams.get("perfectoPassword"));
			
			// Use the automationName capability to defined the required framework - Appium (this is the default) or PerfectoMobile.
			specifiedCapabilities.setCapability("automationName", "PerfectoMobile");
			
			rwdUrl = "https://" + testngParams.get("perfectoCloud") + "/nexperience/perfectomobile/wd/hub";
		}
		
		logThread("Attempting to get driver with: " + specifiedCapabilities.toString());
		RemoteWebDriver driver = null;
		try {
			driver = new RemoteWebDriver(new URL(rwdUrl), specifiedCapabilities);
			logThread("Successfully returned driver for: " 
					+ (driver.getCapabilities().getCapability("deviceName") != null ? 
							driver.getCapabilities().getCapability("deviceName") : 
								driver.getCapabilities().getPlatform() + "," + driver.getCapabilities().getBrowserName()));	
		} catch (Exception e) {
			logThread("FAILED to get driver for:\n\t" + specifiedCapabilities.toString() + "\n\t"
					+ "ERROR: " + e.getMessage().split("\n")[0]);
			Assert.fail(e.getMessage());
		}
			
		return driver;
	}
	
	public void logThread(String msg) {
		System.out.println("Thread-" + Thread.currentThread().getId() + " :" + msg);
	}
	
	public void updateCredsWarning(String whichCred){
		if (testngParams.get(whichCred) != null && testngParams.get(whichCred).contains("YOUR_")) {
			String msg = "!!!Please update the " + whichCred + " parameter in the testng.xml file located at the project root.!!!";
			System.out.println(String.format("%1$s\n%2$s\n%1$s\n", StringUtils.repeat("!",  msg.length()), msg));
			Assert.fail(whichCred + " parameter was not updated in testng.xml file!");
		}
	}
}
